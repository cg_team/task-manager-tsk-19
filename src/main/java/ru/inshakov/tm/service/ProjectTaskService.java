package ru.inshakov.tm.service;

import ru.inshakov.tm.api.repository.IProjectRepository;
import ru.inshakov.tm.api.repository.ITaskRepository;
import ru.inshakov.tm.api.service.IProjectTaskService;
import ru.inshakov.tm.model.Project;
import ru.inshakov.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.findAllTasksByProjectId(projectId);
    }

    @Override
    public Task bindTaskByProjectId(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        return taskRepository.bindTaskByProjectId(projectId,taskId);
    }

    @Override
    public Task unbindTaskByProjectId(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        return taskRepository.unbindTaskByProjectId(projectId,taskId);
    }

    @Override
    public Project removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        taskRepository.removeAllTasksByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

}

