package ru.inshakov.tm.service;

import ru.inshakov.tm.api.IRepository;
import ru.inshakov.tm.api.IService;
import ru.inshakov.tm.exception.empty.EmptyEmailException;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.entity.EntityNotFoundException;
import ru.inshakov.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected final IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public E add(final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        return repository.add(entity);
    }

    @Override
    public E findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public E removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Override
    public void remove(final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        repository.remove(entity);
    }

}


