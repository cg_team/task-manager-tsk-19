package ru.inshakov.tm.command.user;

import ru.inshakov.tm.command.AbstractCommand;

public class UserLogoutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-logout";
    }

    @Override
    public String description() {
        return "log out of system";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

}

