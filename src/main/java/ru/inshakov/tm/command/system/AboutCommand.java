package ru.inshakov.tm.command.system;

import ru.inshakov.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String description() {
        return "Developer info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Vsevolod Inshakov");
        System.out.println("E-MAIL: vinshakov@tsconsulting.com");
    }

}
