package ru.inshakov.tm.repository;

import ru.inshakov.tm.api.repository.IProjectRepository;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public List<Project> findAll(Comparator<Project> comparator) {
        final List<Project> listOfProjects = new ArrayList<>(projects);
        listOfProjects.sort(comparator);
        return listOfProjects;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public Project findOneByName(final String name) {
        for(final Project project: projects) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneByIndex(Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        remove(project);
        return project;
    }

    @Override
    public Project removeOneByName(String name) {
        final Project project = findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        remove(project);
        return project;
    }

}
