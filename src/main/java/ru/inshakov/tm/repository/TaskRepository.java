package ru.inshakov.tm.repository;

import ru.inshakov.tm.api.repository.ITaskRepository;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public List<Task> findAll(Comparator<Task> comparator) {
        final List<Task> listOfTasks = new ArrayList<>(tasks);
        listOfTasks.sort(comparator);
        return listOfTasks;
    }

    @Override
    public Task findOneByProjectId(final String projectId) {
        for (final Task task: tasks) {
            if (projectId.equals(task.getProjectId())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task findOneByName(final String name) {
        for(final Task task: tasks) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String projectId) {
        final List<Task> tasksByProjectId = new ArrayList<>();
        for (final Task task: tasks){
            if (projectId.equals(task.getProjectId())) tasksByProjectId.add(task);
        }
        return tasksByProjectId;
    }

    @Override
    public List<Task> removeAllTasksByProjectId(final String projectId) {
        final List<Task> tasksByProjectId = new ArrayList<>();
        for (final Task task: tasks){
            if (projectId.equals(task.getProjectId())) tasksByProjectId.remove(task);
        }
        return tasksByProjectId;
    }

    @Override
    public Task removeOneByIndex(Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        remove(task);
        return task;
    }

    @Override
    public Task removeOneByName(String name) {
        final Task task = findOneByName(name);
        if (task == null) return null;
        remove(task);
        return task;
    }


    @Override
    public Task bindTaskByProjectId(String projectId, String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskByProjectId(String projectId, String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(null);
        return task;
    }


}
