package ru.inshakov.tm.repository;

import ru.inshakov.tm.api.repository.IUserRepository;
import ru.inshakov.tm.exception.entity.UserNotFoundException;
import ru.inshakov.tm.model.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public List<User> users = new ArrayList<>();

    @Override
    public User findByLogin(String login) {
        for (final User user: entities) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(String email) {
        for (final User user: entities) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public void removeByLogin(String login) {
        final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        remove(user);
    }

}
