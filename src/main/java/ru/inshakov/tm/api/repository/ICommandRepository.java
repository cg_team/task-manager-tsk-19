package ru.inshakov.tm.api.repository;

import ru.inshakov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    void add(AbstractCommand command);

}
