package ru.inshakov.tm.api.repository;

import ru.inshakov.tm.api.IRepository;
import ru.inshakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAll(Comparator<Task> comparator);

    Task findOneByProjectId(String projectId);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    List<Task> findAllTasksByProjectId (String projectId);

    List<Task> removeAllTasksByProjectId(String projectId);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    Task bindTaskByProjectId(String projectId, String taskId);

    Task unbindTaskByProjectId(String projectId, String taskId);


}
