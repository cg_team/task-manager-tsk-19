package ru.inshakov.tm.api.repository;

import ru.inshakov.tm.api.IRepository;
import ru.inshakov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    List<Project> findAll(Comparator<Project> comparator);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

}

