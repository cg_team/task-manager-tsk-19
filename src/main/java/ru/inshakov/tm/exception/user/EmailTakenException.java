package ru.inshakov.tm.exception.user;

public class EmailTakenException extends RuntimeException {

    public EmailTakenException(String email) {
        super("Error! Email ``" + email + "`` is already taken...");
    }

}
