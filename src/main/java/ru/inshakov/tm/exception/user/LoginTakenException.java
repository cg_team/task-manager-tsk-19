package ru.inshakov.tm.exception.user;

public class LoginTakenException extends RuntimeException {

    public LoginTakenException(String login) {
        super("Error! Login ``" + login + "`` is already taken...");
    }

}
