package ru.inshakov.tm.exception.system;

public class UnknownArgumentException extends RuntimeException {

    public UnknownArgumentException(String arg) {
        super("Error! Argument ``" + arg + "`` not found...");
    }

}
