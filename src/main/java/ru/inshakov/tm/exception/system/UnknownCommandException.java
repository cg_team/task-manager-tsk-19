package ru.inshakov.tm.exception.system;

public class UnknownCommandException extends RuntimeException {

    public UnknownCommandException(String command) {
        super("Error! Command ``" + command + "`` not found...");
    }

}
